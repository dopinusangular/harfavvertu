
'use strict';

$(window).on('load', function() {

	$(".loader").fadeOut();
	$("#preloder").delay(800).fadeOut("slow");

	if($('.playlist-area').length > 0 ) {
		var containerEl = document.querySelector('.playlist-area');
		var mixer = mixitup(containerEl);
	}

});

(function($) {

	
    $('.header-right').clone().prependTo('.slicknav_nav > .header-right-warp');

	$('.set-bg').each(function() {
		var bg = $(this).data('setbg');
		$(this).css('background-image', 'url(' + bg + ')');
	});

	


})(jQuery);

