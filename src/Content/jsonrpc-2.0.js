var RpcClient = function() {
    var dt = new Date();
    var _id = dt.getMilliseconds() + (1000 * dt.getSeconds()) + (60000 * dt.getMinutes()) + (3600000 * dt.getHours());
    var _ctxid = 0;
    var _preempted = null;
    var _pendingAjaxCount = 0;

    function schedulePreemptedQuery(id, ticket, processor, progressor, context) {
        var started = true;
        if (!_preempted) {
            _preempted = [];
            started = false;
        }

        _preempted.push({ id: id, ticket: ticket, processor: processor, progressor: progressor, context: context });

        function query() {
            if (_preempted.length == 0) {
                _preempted = null;
                return;
            }

            // Configure the RPC context to automatically reschedule the query afterwards.
            var ctx = RpcClient.context().onFinally(function () {
                setTimeout(query, 5000);
            });

            var tickets = [];
            for (var i = 0; i < _preempted.length; i++) {
                tickets.push(_preempted[i].ticket);
            }

            RpcClient.call('$.QueryPreemptedResponses', tickets).done(function (e) {
                if (e.error) {
                    // Something went wrong. Prevent future queries by removing current items
                    // and manually invoke the callbacks with appropriate error.
                    var items = _preempted.splice(0, tickets.length);

                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.processor.process(e);
                    }

                    if (items.context) {
                        items.context.preempted = null;
                        items.context.__leave();
                    }

                    return false; // Handled.
                }

                for (var ticket in e.result) {
                    var report = e.result[ticket];

                    // Find the pending request that corresponds to current report.
                    var idx = -1;
                    for (var j = 0; j < _preempted.length; j++) {
                        var p = _preempted[j];
                        if (p.ticket == ticket) {
                            idx = j;
                            break;
                        }
                    }

                    if (idx == -1) {
                        // Request not found. Shouldn't happen.
                        continue;
                    }

                    if (report.status == 'error') {
                        // The response could not be found.
                        var item = _preempted.splice(idx, 1)[0];
                        try {
                            item.processor.process({ id: item.id, error: { code: -1, message: report.reason } });
                        } finally {
                            // Clean up the original context.
                            if (item.context) {
                                var idxt = item.context.preempted.indexOf(ticket);
                                item.context.preempted.splice(idxt, 1);

                                if (item.context.preempted.length == 0) {
                                    item.context.preempted = null;
                                    item.context.__leave();
                                }
                            }
                        }

                        continue;
                    }

                    if (report.status == 'ok') {
                        // The response is ready to be consumed.
                        var retrieve = function(r) {
                            RpcClient.call('$.RetrievePreemptedResponse', r.ticket)
                                .done(function(e) {
                                    try {
                                        r.processor.process(e);
                                    } finally {
                                        // Clean up the original context.
                                        if (r.context) {
                                            var idxt = r.context.preempted.indexOf(r.ticket);
                                            r.context.preempted.splice(idxt, 1);

                                            if (r.context.preempted.length == 0) {
                                                r.context.preempted = null;
                                                r.context.__leave();
                                            }
                                        }
                                    }
                                })
                                .post(ctx);
                        }

                        var r = _preempted.splice(idx, 1)[0];
                        retrieve(r);

                        continue;
                    }

                    // Trigger the progress callback.
                    var item = _preempted[idx];
                    if (item.progressor) {
                        item.progressor.progress(report);
                    }
                }

                return false; // Handled.
            }).post(ctx);
        }

        if (!started) {
            setTimeout(query, 5000);
        }
    }

    var _dispatcher = function () {
        var _queue = [];
        var _last = null;
        var _delay = 700; // This is the window in ms, during which rapid requests are batched.

        function postQueue() {
            var queue = _queue;
            _queue = [];

            if (queue.length == 0) {
                return;
            }

            // Combine all batches into one request.
            var batch;
            if (queue.length == 1) {
                batch = queue[0].batch;
            } else {
                batch = [];
                for (var i = 0; i < queue.length; i++) {
                    var qb = queue[i].batch;
                    for (var j = 0; j < qb.length; j++) {
                        batch.push(qb[j]);
                    }
                }
            }

            var request = [];
            if (batch.length == 1) {
                request = batch[0].request;
            }
            else {
                // Send multiple requests as sync batch so server can take advantage of caching.
                request.push({ method: '$.BeginSynchronizedBatch', params: [] });
                for (var i = 0; i < batch.length; i++) {
                    var r = batch[i].request;
                    if (i == 0 && r.method == request[0].method) {
                        // Don't duplicate the sync batch header - use the explicitly provided one.
                        request[0] = r;
                        continue;
                    }

                    request.push(batch[i].request);
                }
            }
            
            // Prepare a joined fail wrapper for all queued batches.
            var fail = function() {
                for (var i = 0; i < queue.length; i++) {
                    queue[i].fail.apply(arguments);
                }
            }

            var config = RpcClient.config;

            var handler = function (data, xhr) {
                try {
                    if (!data) {
                        fail(true, "Server returned an empty response. This is likely a server bug.");
                        return;
                    }

                    if (data.ajaxCallFailed) {
                        if (data.error.status == 'timeout') {
                            fail(true, 'JSON-RPC call failed. Timeout.');
                        } else if (data.error.status == 'error' && !data.error.details) {
                            fail(true, 'JSON-RPC call failed. No connection to server.');
                        } else {
                            fail(true, "JSON-RPC call failed. Status: '" + data.error.status + "'" +
                                (data.error.details ? (", Details: " + JSON.stringify(data.error.details, null, 2)) : ''));
                        }
                        return;
                    }

                    var responses = (typeof data === 'object' && data instanceof Array) ? data : [data];
                    if (responses.length == 0) {
                        fail(true, "Server returned an empty list of responses.");
                        return;
                    }

                    // Look for known special "broadcast"-type responses that come in singles.
                    if (responses.length == 1) {
                        var response = responses[0];
                        if (response.id.indexOf && response.id.indexOf('e2-status-') == 0) {
                            // Distribute this response to all queued requests' callbacks.
                            responses = [];
                            for (var b = 0; b < queue.length; b++) {
                                var qb = queue[b];
                                var batch = qb.batch;
                                for (var m = 0; m < batch.length; m++) {
                                    var copy = $.extend({}, response);
                                    copy.id = batch[m].request.id;

                                    responses.push(copy);
                                }
                            }
                        }
                    }

                    for (var l = 0; l < responses.length; l++) {
                        var response = responses[l];

                        if (!response.id || (typeof (response.result) === 'undefined' && !response.error)) {
                            // TODO: This shouldn't go through fail - it should go through callbacks, otherwise they will hang!
                            fail(true, "Server returned invalid JSON-RPC response: " + JSON.stringify(response, null, 2));
                            return;
                        }

                        // Find the request to which this response belongs.
                        var callbacks = null;
                        var progressCallbacks = null;
                        var context = null;
                        for (var b = 0; b < queue.length; b++) {
                            var qb = queue[b];
                            var batch = qb.batch;
                            for (var m = 0; m < batch.length; m++) {
                                var item = batch[m];
                                if (!item.request.id || (item.request.id != response.id)) {
                                    continue;
                                }

                                callbacks = item.callbacks;
                                progressCallbacks = item.progressCallbacks;
                                context = qb.context;
                                break;
                            }

                            if (callbacks) {
                                break;
                            }
                        }

                        // Prepare a processor instance.
                        var processor = {
                            context: context,
                            callbacks: callbacks,
                            process: function(response, xhr) {
                                // Invoke the context previewer.
                                var doDefaultProcessing = true;
                                if (this.context) {
                                    doDefaultProcessing = this.context.__preview(response);
                                }

                                if (!doDefaultProcessing) {
                                    return;
                                }

                                // Invoke the callbacks.
                                if (this.callbacks) {
                                    for (var n = 0; n < this.callbacks.length; n++) {
                                        var callback = this.callbacks[n];
                                        if (!callback(response, xhr)) {
                                            doDefaultProcessing = false;
                                        }
                                    }
                                }

                                // If none of the handlers actually handled the request, invoke our default processing.
                                if (doDefaultProcessing) {
                                    if (response.error) {
                                        if (!this.context || !this.context.__defaultApiFail(response.error)) {
                                            fail(false, response.error);
                                        }
                                    } else if (!this.context || !this.context.__defaultApiSucceed(response.result)) {
                                        config.log(response.result);
                                    }
                                }
                            }
                        };

                        // Response was preempted. Schedule processor for background waiting.
                        if (response.error && response.error.code == -32099) {
                            var ticket = response.error.data;

                            if (context) {
                                if (!context.preempted) {
                                    context.preempted = [];
                                }

                                context.preempted.push(ticket);
                            }

                            // Prepare a progress handler.
                            var progressor = {
                                callbacks: progressCallbacks,
                                progress: function(report) {
                                    if (!this.callbacks) {
                                        return;
                                    }

                                    for (var c = 0; c < this.callbacks.length; c++) {
                                        var callback = this.callbacks[c];
                                        if (callback) {
                                            callback(report);
                                        }
                                    }
                                }
                            };
                            
                            schedulePreemptedQuery(response.id, ticket, processor, progressor, context);
                            continue;
                        }

                        // Process the current response.
                        processor.process(response, xhr);
                    }
                } finally {
                    for (var i = 0; i < queue.length; i++) {
                        var ctx = queue[i].context;
                        if (ctx && !ctx.preempted) {
                            // Only leave the context at this point if none of its requests were preempted!
                            ctx.__leave();
                        }
                    }
                }
            };

            var ajaxConfig = {
                url: config.url,
                type: 'POST',
                async: true,
                cache: false,
                timeout: config.timeout,
                global: true, // This allows event procesing on the document level.
                contentType: 'application/json', // This is what we're sending to server...
                dataType: 'json', // ...and this is what we're expecting to get back.
                processData: false, // No processing, please. We're sending a JSON string.
                data: JSON.stringify(request),
                error: function (xhr, status, e) {
                    _pendingAjaxCount--;
                    handler({ error: { status: status, details: e }, ajaxCallFailed: true });
                },
                success: function (data, status, xhr) {
                    _pendingAjaxCount--;
                    handler(data, xhr);
                }
            };
            
            // Is this request supposed to go as CORS?
			if (config.cors) {
				if ($.support.cors) {
                    ajaxConfig.crossDomain = true;
                    ajaxConfig.xhrFields = {
						withCredentials : true
					}                        				
				} else {
					alert("ERROR: Browser doesn't support CORS!");
					return;
				}
			}
            
            // Apply custom headers.
            var headers = config.headers;
            if (!headers) {
                headers = {};
            }

            // Apply E3 API mapping header.
            if (!config.disableE3DataApiMapping) {
                headers["Use-E3-DataApi"] = 1;
            }

            // Configure preemptive response support.
            var preempt = headers["JsonRpc-Preempt-After"];
            if (!preempt) {
                headers["JsonRpc-Preempt-After"] = config.jsonRpcPreemptAfter;
            }

            ajaxConfig["headers"] = headers;

            _pendingAjaxCount++;

            $.ajax(ajaxConfig);
        }

        return {
            dispatch: function(batch, context, fail) {
                var last = _last;
                _last = new Date();

                if (_queue.length == 0) {
                    _queue.created = _last;
                }

                _queue.push({ batch: batch, context: context, fail: fail });

                // Make sure that preemption requests aren't batched. It would be a protocol violation.
                if (batch.length == 1) {
                    var method = batch[0].request.method;     
                    if (method[0] == '$' && method.indexOf('Preempted') > -1) {
                        //If preemption request comes in a batch it's popped from the queue and sent separately after the batch.
                        if (_queue.length > 1) {
                            var tmpBatch = _queue[(_queue.length)-1];
                            _queue.pop();
                            postQueue(); 
                            
                            _queue = [];
                            _queue.push(tmpBatch);                            
                        }
                        //Solo preemption requests are sent straight away.
                        postQueue();
                        return;
                    }
                }

                if (last != null) {
                    var diff = _last - last;
                    var span = _last - _queue.created;

                    var delayExistingBatch = span < 3000 && (_pendingAjaxCount > 3 || diff < _delay);
                    if (delayExistingBatch) {
                        // Reschedule pending post.
                        if (_queue.timeoutHandle) {
                            clearTimeout(_queue.timeoutHandle);
                            _queue.timeoutHandle = null;
                        }
                    }
                }

                if (_pendingAjaxCount == 0) {
                    // There are no pending AJAX posts, so post immediately.
                    _queue.timeoutHandle = setTimeout(postQueue, 1);
                    return;
                }

                // There are existing pending posts, so schedule a delayed one.
                _queue.timeoutHandle = setTimeout(postQueue, _delay + 50);
            }
        }
    }();

    function preparePacket() {
        var method = arguments[0];
        var args = [];

        if (arguments.length == 2 && $.isPlainObject(arguments[1])) {
            args = arguments[1];
        } else {
            for (var i = 1; i < arguments.length; i++) {
                args.push(arguments[i]);
            }
        }

        return { method: method, params: args };
    }

    function main(packet, config) {
        var batch = [];
        batch.push({ request: packet, callbacks: [], progressCallbacks: [] });

        var context = null;
        
        var fail = function() {
            // NOTE: Context.__rpcFail should only be called for ajax failures.
            if (context && arguments[0] === true && context.__rpcFail(arguments[1])) {
                return;
            }
            
            if (config && config.unhandledError) {
                config.unhandledError(arguments[1]);
                return;
            }
            
            console.error.apply(console, arguments);
        };

        return {
            call: function () {
                var p = preparePacket.apply(null, arguments);
                _id++;
                p.id = _id;
                batch.push({ request: p, callbacks: [], progressCallbacks: [] });
                
                return this;
            },
            notify: function () {
                var p = preparePacket.apply(null, arguments);
                batch.push({ request: p, callbacks: [], progressCallbacks: [] });
                
                return this;
            },
            using: function (id) {
                var p = batch[batch.length - 1].request;
                p.id = id;

                return this;
            },
            done: function (c) {
                var item = batch.length > 0 ? batch[batch.length - 1] : null;
                if (typeof (c) == 'function') {
                    item.callbacks.push(c);
                }

                return this;
            },
            progress: function (c) {
                var item = batch.length > 0 ? batch[batch.length - 1] : null;
                if (typeof (c) == 'function') {
                    item.progressCallbacks.push(c);
                }

                return this;
            },
            post: function (ctx) {
                context = ctx;
                if (context) {
                    context.__enter();
                }

                if (!config) {
                    try {
                        fail(true, "RpcClient was invoked without valid config object!");
                    } finally {
                        if (context) {
                            context.__leave();
                        }
                    }
                    return;
                }

                // Is this request supposed to go as JSONP?
                if (config.jsonp) {
                    var request = [];
                    if (batch.length == 1) {
                        request = batch[0].request;
                    }
                    else {
                        for (var i = 0; i < batch.length; i++) {
                            request.push(batch[i].request);
                        }
                    }

                    var headers = config.headers;
                    if (!headers) {
                        headers = {};
                    }

                    var jsonp = "__jsonpCallback_" + _id;
                    var url = config.url + "?callback=" + jsonp + "&jsonrpc=" + JSON.stringify(request);
                    
                    for (var header in headers) {
                        url = url + "&" + header + "=" + config.headers[header];
                    }

                    var script = document.createElement('script');
                    script.setAttribute('src', url);

                    window[jsonp] = function (data) {
                        window[jsonp] = null;

                        document.getElementsByTagName('body')[0].removeChild(script);
                        script = null;

                        handler(data);
                    };

                    // This will trigger the call to JSONP server.
                    document.getElementsByTagName('body')[0].appendChild(script);
                    
                    if (context) {
                        context.__leave();
                    }

                    return;
                }

                // No, dispatch it through AJAX.
                _dispatcher.dispatch(batch, context, fail);
            }
        };
    }

    return {
        config: {
            url: 'json.rpc',
            timeout: 20000,
            jsonRpcPreemptAfter: 15,
            disableE3DataApiMapping: false,
            log: function() { console.log(arguments[1]); },
            unhandledError: function() { console.error(arguments[1]); }
        },
        call: function() {
            var packet = preparePacket.apply(null, arguments);
            _id++;
            packet.id = _id;
            return main(packet, this.config);
        },
        notify: function () {
            var packet = preparePacket.apply(null, arguments);
            return main(packet, this.config);
        },
        context: function () {
            _ctxid++;

            var ctx = {
                id: _ctxid,
                count: 0,
                rpcErrorHandlers: [],
                apiErrorHandlers: [],
                apiSuccessHandlers: [],
                responsePreviewers: [],
                finallyHandlers: []
            };

            return {
                // These are meant for user code.
                onRpcError: function (c) {
                    if (typeof (c) == 'function') {
                        ctx.rpcErrorHandlers.push(c);
                    }

                    return this;
                },
                onDefaultApiError: function (c) {
                    if (typeof (c) == 'function') {
                        ctx.apiErrorHandlers.push(c);
                    }

                    return this;
                },
                onDefaultApiSuccess: function (c) {
                    if (typeof (c) == 'function') {
                        ctx.apiSuccessHandlers.push(c);
                    }

                    return this;
                },
                onFinally: function (c) {
                    if (typeof (c) == 'function') {
                        ctx.finallyHandlers.push(c);
                    }

                    return this;
                },
                onPreviewResponse: function (c) {
                    if (typeof (c) == 'function') {
                        ctx.responsePreviewers.push(c);
                    }

                    return this;
                },

                // These are called internally.
                __enter: function () {
                    ctx.count++;
                },
                __preview: function () {
                    // IMPORTANT: If this function returns true, it means it DIDN'T handle the request and other handlers should proceed.
                    //            This is the reverse of the other function below, which return true when they DO handle it!
                    if (ctx.responsePreviewers.length == 0) {
                        return true; // Not handled.
                    }

                    var handled = false;

                    for (var i = 0; i < ctx.responsePreviewers.length; i++) {
                        var callback = ctx.responsePreviewers[i];
                        if (!callback.apply(null, arguments)) {
                            handled = true;
                        }
                    }

                    return !handled;
                },
                __rpcFail: function () {
                    if (ctx.rpcErrorHandlers.length == 0) {
                        return false;
                    }

                    for (var i = 0; i < ctx.rpcErrorHandlers.length; i++) {
                        var callback = ctx.rpcErrorHandlers[i];
                        callback.apply(null, arguments);
                    }

                    return true;
                },
                __defaultApiFail: function () {
                    if (ctx.apiErrorHandlers.length == 0) {
                        return false;
                    }

                    for (var i = 0; i < ctx.apiErrorHandlers.length; i++) {
                        var callback = ctx.apiErrorHandlers[i];
                        callback.apply(null, arguments);
                    }

                    return true;
                },
                __defaultApiSucceed: function () {
                    if (ctx.apiSuccessHandlers.length == 0) {
                        return false;
                    }

                    for (var i = 0; i < ctx.apiSuccessHandlers.length; i++) {
                        var callback = ctx.apiSuccessHandlers[i];
                        callback.apply(null, arguments);
                    }

                    return true;
                },
                __leave: function () {
                    ctx.count--;
                    if (ctx.count > 0) {
                        return;
                    }

                    for (var i = 0; i < ctx.finallyHandlers.length; i++) {
                        var callback = ctx.finallyHandlers[i];
                        callback.apply(null, arguments);
                    }
                }
            };
        }
    };
}();