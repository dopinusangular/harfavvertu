/**
* This class requires RpcClient class to operate (jsonrpc.js)
*
*
*/
/****************************************************************************************************/
/*******************************Authenticate service helpers (Requires Sha1 class)***************/
/****************************************************************************************************/
function E2Auth(){
    this.username = null;
    this.password = null;
    this.orgid=null;
    //User with limited functionality
    this.anonymous = false;

    this.persist = false;

    this.callback = null;
    this.cbCtx = null;
}

E2Auth.prototype.Authenticate = function(){
    if(this.anonymous)
        this.username = "nezaposlen";

    if(!this.username){
        console.error("No username");
        if(this.callback)
            this.callback.call(this.cbCtx||this, null);
    }

    if(!this.password && !this.anonymous){
        console.error("No password");
        if(this.callback)
            this.callback.call(this.cbCtx||this, null);
    }

    var ctx = getStdRpcContext().onFinally(function(){
    });

    var that = this;

    RpcClient.call("Auth.IdentifyUsername", this.username).done(function(e){
        var challenge = e.result;
        var fixedHash = "";
        if(that.anonymous){
            fixedHash = "56ba33efeafff7b56a0df775fbac1c872dbf4ef5";
        }else{
            var fixedSalt = Sha1.hash(naiveReverse(that.username),true);
            fixedHash = Sha1.hash(that.password+fixedSalt, true);
        }

        var proof = Sha1.hash(fixedHash+challenge);
        const autObj = {challenge : challenge, proof : proof, persist : that.persis};
        if(this.orgid !== null)
        {
          autObj["orgid"]=this.orgid;
        }
        RpcClient.call("Auth.AuthenticateUsername", autObj).done(function(e){
            if(!that.callback)
                return;
            if(e.result == "ok"){
                that.callback.call(that.cbCtx||that, true);
            }else{
                that.callback.call(that.cbCtx||that, false);
            }
        }).post(ctx);
    }).post(ctx);
}

E2Auth.prototype.SignOut = function(){
    var ctx = getStdRpcContext().onFinally(function(){

    });
    var that = this;

    RpcClient.call("Auth.SignOut").done(function(e){
        if(!that.callback)
            return;
        if(e.result){
            that.callback(that.cbCtx||that, true);
        }else{
            that.callback(that.cbCtx||that, false);
        }
    }).post(ctx);
}
E2Auth.prototype.GetSessionInfo = function(){
  var ctx = getStdRpcContext().onFinally(function(){

  });
  var that = this;

  RpcClient.call("Auth.GetSessionInfo",true).done(function(e){
      if(!that.callback)
          return;
      if(e.result){
          that.callback(that.cbCtx||that, e);
      }else{
          that.callback(that.cbCtx||that, e);
      }
  }).post(ctx);
}
E2Auth.prototype.IsAuth = function(){
  var ctx = getStdRpcContext().onFinally(function(){

  });
  var that = this;

  RpcClient.call("Auth.GetSessionInfo",true).done(function(e){
      if(!that.callback)
          return;
      if(e.result){
          that.callback(that.cbCtx||that, true);
      }else{
          that.callback(that.cbCtx||that, false);
      }
  }).post(ctx);
}
/**
* Will cause issues in spcific characters (https://mathiasbynens.be/notes/javascript-encoding)
*
*/
function naiveReverse(string){
    return string.split('').reverse().join('');
}
/**************************************************************************/
/*******************************Data service helpers***********************/
/**************************************************************************/
function E2Data(){
    /**
	Entity in which the data should be stored*/
    this.entity = null;
    /**
		Object holding input values*/
    this.values = null;
    /**
		Request expression*/
    this.expressions = null;
    /**
		List of fields to select*/
    this.select = null;
    /**
		Number of records to skip*/
    this.skip = null;
    /**
        Number of records to take after skipped
		 */
    this.take = null;
    /**
		Include count in the response*/
    this.includeCount = null;
    /**
		Component id*/
    this.component = null;
    /**
    * Value of the switch field
    */
    this.extSwitchFieldValue = null;
    /**
		Format of data to return (Flat, Full)*/
    this.format = null;
    /**
		Filter as logical expression*/
    this.filter = null;
    /**
        Order by fileds csv*/
    this.orderBy = null;
    this.nulls=false;
    /**
    * Callback functon
    */
    this.callback = null;
    /**
      Callback context*/
    this.cbCtx = null;
}

E2Data.prototype.CreateData = function(){
    var method = "Data.Create";
    var _fromat = this.format || "Flat";
    var _values = this.values || {};
    var params = {
        "entity" : this.entity,
        "values" : _values,
        "format" : _fromat
    }

    var that = this;
    RpcClient.call(method, params).done(function(response){
        if(that.callback){
            that.callback.call(that.cbCtx||that, response);
        }
    }).post(getStdRpcContext());
}

E2Data.prototype.GetData = function(){
    var method = "Data.GetEntityData";

    var params = {};

    params.entity = this.entity;
    params.skip = isNaN(this.skip) ? 0 : this.skip;
    params.take = isNaN(this.take) ? 100 : this.take;
    params.format = this.format || "Flat";

    if(this.filter)
        params.filter = this.filter;
    if(this.select)
        params.select = this.select;
    if(this.orderBy)
        params.orderBy = this.orderBy;
    if(this.includeCount)
        params.includeCount = true;
    if(this.component)
        params.component = this.component;
    if(this.extSwitchFieldValue)
        params.extSwitchFieldValue = this.extSwitchFieldValue;
    if(this.nulls)
    {
      params['@nulls']=true
    }
    var that = this;
    RpcClient.call(method,params).done(function(response){
        if(that.callback){
            that.callback.call(that.cbCtx||that, response);
        }
    }).post(getStdRpcContext());
}

E2Data.prototype.GetEntityInfo = function(){
    var method = "Data.GetEntityInfo";
    var params = {};

    params.entity = this.entity;
    params.format = this.format || "Flat";

    var that = this;
    RpcClient.call(method,params).done(function(response){
        if(that.callback){
            that.callback.call(that.cbCtx||that, response);
        }
    }).post(getStdRpcContext());
}

E2Data.prototype.ModifyData = function(){
    var method = "Data.Modify";
    var params = {};

    params.entity = this.entity;
    params.filter = this.filter;
    var _values = this.values || {};
    params.values = _values;
    params.format = this.format || "Flat";
    if(this.expressions)
        params.expressions = this.expressions;

    var that = this;
    RpcClient.call(method,params).done(function(response){
        if(that.callback){
            that.callback.call(that.cbCtx||that, response);
        }
    }).post(getStdRpcContext());
}

E2Data.prototype.DeleteData = function(){
    var method = "Data.Modify";
    var params = {};

    params.entity = this.entity;
    params.filter = this.filter;
    params.format = this.format || "Flat";

    var that = this;
    RpcClient.call(method,params).done(function(response){
        if(that.callback){
            that.callback.call(that.cbCtx||that, response);
        }
    }).post(getStdRpcContext());
}



function getStdRpcContext() {
    return RpcClient.context().onRpcError(function (e) {
    }).onDefaultApiError(function (e) {
    }).onDefaultApiSuccess(function (res) {
    });
}

/**************************************************************************/
/*******************************Entity as tree extension*******************/
/**************************************************************************/

function EntityAsTree(){
    /**
        <required><string> Entity name or id*/
    this.entity = null;
    /**
        <string> Parent field name over which the values are connected in this entity*/
    this.parent = null;
    //Optional

    /**
        <string> Id of the starting node aka manual root*/
    this.node = null;
    /**
        <string> Field names we wish returned. Default returns all. Is in CSV format*/
    this.select = null;
    /**
        <string> Entity filter*/
    this.filter = null;
    /**
        <int> Number of level depth we wish to return. Default returns all.*/
    this.depth = null;
    /**
        <string> Field name of the connection from the relationship table to the main entity*/
    this.relationsConnField = null;
    /**
        <string> Field name where the ids of the tree are written in. Is used for filtering. Not required, but will speed up things*/
    this.relationsSourceField = null;
     //Detail table general info

    /**
        <string> Child/detail entity name or id*/
    this.nextLevelEntity = null;
    /**
        <string> Entity filter*/
    this.nextLevelFilter = null;
    /**
        <string> Field names we wish returned. Default returns all. Is in CSV format*/
    this.nextLevelSelect = null;
    /**
        <required with next level entity><string> Field name through which the child entity is connected to the master entity*/
    this.nextLevelEntityConnField = null;
    /**
        Nodes and details are connected through a detail table*/
    this.nextLevelRelationsTable = null;
     /**
        Filter on the table (usually we will filter by the CCIT tree id)*/
    this.nextLevelRElationsFilter = null;
    /**
        <bool> Return with a virtual root. Default is true*/
    this.includeVirtualRoot = true;
     /**
        <bool> Auto inserts a root. Default is false. In the condition of parent field AND filter do not return any values, do we wish to insert a root value*/
    this.autoInsertRoot = false;
    /**
        <bool> Default is false. Values that are no assigned to the current tree and meet the filter requirements, are returned as unasigned values*/
    this.includeUnassginedValues = false;
    /**
        <bool> Default is false. Values that are no assigned to the current tree and meed the filter requirements, are returned as unasigned values*/
    this.includeUnassignedNextLevelValues = false;
    /**
        <guid> CCIT or CIT id of the tree*/
    this.treeId = null;
    /**
        <string> Field name of where the tree id is written*/
    this.treeIdKey = null;
    /**
        <string> Field name of the field where tree id is written in the relations table"*/
    this.treeRelationsIdKey = null;

    this.callback = null;
    this.cbCtx = null;
}

EntityAsTree.prototype.Execute = function(){
    var method = "Extension.Execute";
    var extension = "EntityAsTree";
    var params = {};

    params.extension = extension;
    params.args = this._getBuiltArguments();

    var that = this;
    RpcClient.call(method,params).done(function(response){
        if(that.callback){
            that.callback.call(that.cbCtx||that, response);
        }
    }).post(getStdRpcContext());
}

EntityAsTree.prototype._getBuiltArguments = function(){
    var args = {};

    if(this.entity)
        args.entity = this.entity;

    if(this.parent)
        args.parent = this.parent;

    if(this.node)
        args.node = this.node;

    if(this.select)
        args.select = this.select;

    if(this.filter)
        args.filter = this.filter;

    if(!isNaN(this.depth))
        args.depth = this.depth;

    if(this.relationsSourceField)
        args.relationsSourceField = this.relationsSourceField;

    if(this.relationsConnField)
        args.relationsConnField = this.relationsConnField;

    if(this.nextLevelEntity)
        args.nextLevelEntity = this.nextLevelEntity;

    if(this.nextLevelFilter)
        args.nextLevelFilter = this.nextLevelFilter;

    if(this.nextLevelSelect)
        args.nextLevelSelect = this.nextLevelSelect;

    if(this.nextLevelEntityConnField)
        args.nextLevelEntityConnField = this.nextLevelEntityConnField;

    if(this.nextLevelRelationsTable)
        args.nextLevelRelationsTable = this.nextLevelRelationsTable;

    if(this.treeId)
        args.treeId = this.treeId;

    if(this.treeIdKey)
        args.treeIdKey = this.treeIdKey;

    if(this.treeRelationsIdKey)
        args.treeRelationsIdKey = this.treeRelationsIdKey;

    args.includeVirtualRoot = this.includeVirtualRoot;
    args.autoInsertRoot = this.autoInsertRoot;
    args.includeUnassginedValues = this.includeUnassginedValues;
    args.includeUnassignedNextLevelValues = this.includeUnassignedNextLevelValues;

    return args;
}
function E2Extension(){
    /**
        <required><string> Entity name or id*/
    this.extension = null;
    /**
        <string> Parent field name over which the values are connected in this entity*/
    this.args = null;

    this.callback = null;
    this.cbCtx = null;
}

E2Extension.prototype.Execute = function(){
    var method = "Extension.Execute";
    var params = {};

    params.extension = this.extension;
    params.args = this.args;

    var that = this;
    RpcClient.call(method,params).done(function(response){
        if(that.callback){
            that.callback.call(that.cbCtx||that, response);
        }
    }).post(getStdRpcContext());
}
