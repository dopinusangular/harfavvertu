function E2Browse(){

    this.name = "";
    this.browse = null;
    this.icMemory = null;
    this.resizeSenzor = null;
    this.resizeHeightSenzor = null;
    this.inlineContainer = null;

    this.componentInfo = null;
    this.tableInfo = null;
    this.tableMetaData = null;

    this.inlineContainerId = null;
    this.resizeContainerId = null;
    this.resizeHeightId = null;
    this.resizeHeightAdjust = 0;

}

E2Browse.prototype.show = function(){
    this.getInlineContainer().add(this.getBrowse());
}

E2Browse.prototype.build = function(cb, ctx){
    this.getBrowse();
    this.getMemoryLinker();
    this.getInlineContainer();
    this.getResizeSenzor();
    this.getResizeHeightSenzor();
    this.registerMemoryLinker();
}
/**
*
*
*/
E2Browse.prototype.createNewRecord = function(values, cb, ctx){
    if(!values)
        return cb.call(ctx||this, false);

    var e2Data = new E2Data();
    e2Data.entity =  this.tableInfo.entity;
    e2Data.values = values;
    e2Data.callback = function(e){
        if(e && e.result && e.result.values && e.result.values.length > 0 && e.result.values[0].id)
            return cb.call(ctx||this,true, e.result.values[0].id)

        cb.call(ctx||this, false);
    };
    e2Data.cbCtx = this;
    e2Data.CreateData();
}
/**
*
*
*/
E2Browse.prototype.modifySelectedRow = function(values, cb,ctx){
    if(!values)
        return cb.call(ctx||this, false);

    if(uil.misc.Generic.isStringEmpty(this.getSelectedRowData()["Id"]))
        return cb.call(ctx||this, false);

    var e2Data = new E2Data();
    e2Data.entity =  this.tableInfo.entity;
    e2Data.values = values;
    e2Data.filter = 'Id="'+this.getSelectedRowData().Id+'"';
    e2Data.callback = function(e){
        this.browse.getTable().getTableModel().reloadData();
        if(e && e.result && e.result.status == "ok")
            return cb.call(ctx||this, true);
        if(e && e.error && e.error.data)
            return cb.call(ctx||this, false, e.error.data);

        cb.call(ctx||this, false, "Sprememba ni bila shranjena. Napaka neznana");
    };
    e2Data.cbCtx = this;
    e2Data.ModifyData();
}
/**
* Get the current selected row in the table
*
*@return {object}
*/
E2Browse.prototype.getSelectedRowData = function(){
    if(!this.getBrowse())
        return {};

    var row = this.getBrowse().getTable().getFocusedRow();
    if(!qx.lang.Type.isNumber(row))
        return {};

    var data = this.getBrowse().getTable().getTableModel().getRowData(row);
    if(!data)
        return {};

    return data;
}
/**
* Registers this memory linker to the global "ciri" component
*
*/
E2Browse.prototype.registerMemoryLinker = function(){
    var componentMemoryLinker = uil.components.execute.memoryICLinker.ComponentCommunicator.getInstance().findComponentById("ciri");
    if(!componentMemoryLinker)
        return false;

    componentMemoryLinker.addInternalComponent(this.getMemoryLinker());
}
/**
*
*
*@return {uil.components.execute.memoryICLinker.internalComponents.Browse}
*/
E2Browse.prototype.getMemoryLinker = function(){
    if(this.icMemory)
        return this.icMemory;

    var icMemory = new uil.components.execute.memoryICLinker.internalComponents.Browse();
    icMemory.setInternalComponentWidget(this.getBrowse());
    icMemory.setId(this.name);
    this.icMemory = icMemory;
    return this.icMemory;
}
/**
*
*
*@return {uil.components.browse.Browse}
*/
E2Browse.prototype.getBrowse = function(){
    if(this.browse)
        return this.browse;

    if(!this.componentInfo || !this.tableInfo || !this.tableMetaData){
        console.error("No component info found. Stopping");
        return null;
    }

    var browse = new uil.components.browse.Browse(this.tableInfo, this.tableMetaData);
    var scroller = browse.getTable().getPaneScroller(0);
    scroller.getChildControl("focus-indicator").setOverrideRowHeight(19);

    this.browse = browse;
    this.__attachBrowseListiners();

    return this.browse;
}

/**
* Get the inline container for the browse
*
*@return {qx.ui.root.Inline}
*/
E2Browse.prototype.getInlineContainer = function(){
    if(this.inlineContainer)
        return this.inlineContainer;

    this.inlineContainer = new qx.ui.root.Inline(document.getElementById(this.inlineContainerId));
    return this.inlineContainer;
}
/**
*
*
*/
E2Browse.prototype.getResizeSenzor = function(){
    if(this.resizeSenzor)
        return this.resizeSenzor;

    var el = document.getElementById(this.resizeContainerId);
    var that = this;

    this.resizeSenzor = new ResizeSensor(el, function() {
        if(that.getBrowse())
            that.getBrowse().setWidth(el.clientWidth);
    });

    return this.resizeSenzor;
}
E2Browse.prototype.resetHeight = function(){
    if(!this.resizeHeightId)
        return;

    var el = document.getElementById(this.resizeHeightId);
    if(this.getBrowse())
        this.getBrowse().setHeight(el.clientHeight+this.resizeHeightAdjust);
}
/**
*
*
*/
E2Browse.prototype.getResizeHeightSenzor = function(){
    if(!this.resizeHeightId)
        return null;

    if(this.resizeHeightSenzor)
        return this.resizeHeightSenzor;

    var el = document.getElementById(this.resizeHeightId);
    var that = this;

    this.resizeHeightSenzor = new ResizeSensor(el, function() {
        if(that.getBrowse())
            that.getBrowse().setHeight(el.clientHeight+that.resizeHeightAdjust);
    });

    return this.resizeHeightSenzor;
}

E2Browse.prototype.__onBrowseReady = function(){
    this.__hideToolbarButtons();
}

E2Browse.prototype.__attachBrowseListiners = function(){
    if(this.browse.getReady()){
        this.__onBrowseReady();
    }else{
        this.browse.addListenerOnce("changedReady", function(){this.__onBrowseReady()},this);
    }
}


E2Browse.prototype.__hideToolbarButtons = function(){
    if(!this.getBrowse())
        return;

    this.getBrowse().getToolbar().getChildren()[0].exclude();
    this.getBrowse().getToolbar().getChildren()[2].exclude();
    this.getBrowse().getToolbar().getChildren()[3].exclude();
    this.getBrowse().getToolbar().getChildren()[4].exclude();
    this.getBrowse().getToolbar().getChildren()[5].exclude();
    this.getBrowse().getToolbar().getChildren()[6].exclude();
    this.getBrowse().getToolbar().getChildren()[15].exclude();
}
