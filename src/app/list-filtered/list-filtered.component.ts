import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotifyChangesService } from '../notify-changes.service';
@Component({
  selector: 'app-list-filtered',
  templateUrl: './list-filtered.component.html',
  styleUrls: ['./list-filtered.component.css']
})
export class ListFilteredComponent implements OnDestroy {
  poetry = { data: [] };
  poetryObj = null;
  subscription: Subscription;

  listEmpty = false;

  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscription = this.notfychangeservice.reloadingPoetry().subscribe(poetry => {
      this.poetry = poetry;
      this.poetryObj = {};
      if (this.poetry.data.length > 0) {
        this.listEmpty = false;
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.poetry.data.length; i++) {
          this.poetry.data[i].classimage = 'audio-img';
          if (this.poetry.data[i].Vrsta === '384103d4-3f57-11ea-8116-81991b6bff66') {
            this.poetry.data[i].classimage = 'video-img';
          }
          this.poetryObj[this.poetry.data[i].Id] = this.poetry.data[i];
        }
      } else {
        this.poetry = { data: [] };
        this.poetryObj = null;
        this.listEmpty = true;
      }

    });

  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  changePoetry(id) {
    const obj = this.poetryObj[id];
    this.notfychangeservice.changeActivePoetry(obj);
  }
  identify(index, item) {
    return item.name;
  }
  changeDetail(id) {
    const obj = this.poetryObj[id];
    this.notfychangeservice.changeDetail(obj);
  }
}
