import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFilteredComponent } from './list-filtered.component';

describe('ListFilteredComponent', () => {
  let component: ListFilteredComponent;
  let fixture: ComponentFixture<ListFilteredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListFilteredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFilteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
