import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotifyChangesService } from '../notify-changes.service';
import { PoetrySource } from '../poetry-source';
declare const $: any;
declare const jQuery: any;
declare const WaveSurfer: any;
@Component({
  selector: 'app-current-playing',
  templateUrl: './current-playing.component.html',
  styleUrls: ['./current-playing.component.css']
})
export class CurrentPlayingComponent implements OnDestroy {
  poetry: PoetrySource;
  poetryUrl = null;
  wavesurfer = null;
  subscription: Subscription;
  subscriptionTran: Subscription;
  tran: {Interpretation: null};
  constructor(private notfychangeservice: NotifyChangesService) {
    this.poetry = new PoetrySource(null , null);
    this.subscription = this.notfychangeservice.changePoetryPlaying().subscribe(poetry => {
      this.poetry = new PoetrySource(poetry, this.notfychangeservice.getBaseUrl());
      this.changePlayPoetry();
    });
    this.subscriptionTran = this.notfychangeservice.reloadLangModel().subscribe(tran => {
      this.tran = tran;
    });
    if (this.notfychangeservice.getTranData() !== null) {
      this.tran = this.notfychangeservice.getTranData();
    }


  }
  ngOnDestroy() {
    console.log('destory current')
    this.subscription.unsubscribe();
    this.subscriptionTran.unsubscribe();
    if(this.wavesurfer !== null) {
      this.wavesurfer.pause();
    }
  }

  wavesurferInit() {
    const wavesurfer = this.wavesurfer;
    const that = this;
    $(document).ready(() => {
      $('#currentTime').appendTo('wave wave');
    });

    wavesurfer.on('audioprocess', () => {
      const clipCurrentTime = wavesurfer.getCurrentTime();
      document.getElementById('currentTime').innerHTML = that.formatSecondsAsTime(clipCurrentTime);
    });

    wavesurfer.on('ready', () => {
      const clipTime = wavesurfer.getDuration();
      document.getElementById('clipTime').innerHTML = that.formatSecondsAsTime(clipTime);
      wavesurfer.play();
      console.log('waveplaycurrent')
    });

    wavesurfer.on('interaction', () => {
      const clipCurrentTime = wavesurfer.getCurrentTime();
      document.getElementById('currentTime').innerHTML = that.formatSecondsAsTime(clipCurrentTime);
    });

    wavesurfer.on('play', () => {
      $(document).ready(() => {
        $('.play-box').addClass('playing');
        $('.jplayer').jPlayer('pauseOthers');
      });
    });

    wavesurfer.on('pause', () => {
      $(document).ready(() => {
        $('.play-box').removeClass('playing');
      });
    });
  }
  formatSecondsAsTime(secs: any) {
    const hr = Math.floor(secs / 3600);
    const min = Math.floor((secs - (hr * 3600)) / 60);
    const sec = Math.floor(secs - (hr * 3600) - (min * 60));
    let smin = '' + min;
    const shr = '' + hr;
    let ssec = '' + sec;
    if (min < 10) {
      smin = '0' + min;
    }
    if (sec < 10) {
      ssec = '0' + sec;
    }
    return smin + ':' + ssec;

  }
  changePlayPoetry() {
    this.poetryUrl = this.poetry.stream;
    if (this.wavesurfer === null) {
      const that = this;
      $('#playButton').removeClass('no-wave');
      $(document).ready(() => {
        that.wavesurfer = WaveSurfer.create({
          container: '#audiowave',
          waveColor: '#d0d7db',
          progressColor: '#9ca1a4',
          barWidth: 2,
          barGap: 1,
          cursor: true,
          responsive: true,
          cursorWidth: 0,
          height: 80,
          barHeight: 0.7,
        });
        that.wavesurferInit();
        that.wavesurfer.load(this.poetryUrl);
      });

    } else {
      this.wavesurfer.load(this.poetryUrl);
    }

  }
  playpausewave() {
    this.wavesurfer.playPause();
  }
}
