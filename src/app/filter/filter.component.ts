import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotifyChangesService } from '../notify-changes.service';
declare const $: any;
declare const jQuery: any;
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnDestroy {
  filterStruct = null;
  isVisible = true;
  langArray = [];
  stateArray = [];
  poetArray = [];
  modelFilter = { search: '', lang: null, poet: null, state: null, autors: {} };
  poetObj = {};
  subscriptionTran: Subscription;
  authorVisible = {};
  orderObj = { pesnik: 'asc', pesem: 'asc', interpret: 'asc' }

  tran = { Search: null, SearchPlaceholder: null, ChooseLangContent: null, ChooseStateOfPerfomance: null, Poet: null };
  constructor(private notfychangeservice: NotifyChangesService) {
    this.GetFilterStruct();
    this.subscriptionTran = this.notfychangeservice.reloadLangModel().subscribe(tran => {
      this.tran = tran;
    });
    if (this.notfychangeservice.getTranData() !== null) {
      this.tran = this.notfychangeservice.getTranData();
    }
    if (this.notfychangeservice.getFromLocalStorage('filterModel') !== null) {
      this.modelFilter = this.notfychangeservice.getFromLocalStorage('filterModel');
    }
  }
  ngOnDestroy() {
  }
  GetFilterStruct() {
    const that = this;
    const callback = (response) => {
      if (response == null) {
        return;
      }
      if (response.result) {
        if (response.result.status === 'ok') {
          const struct = response.result.data;
          that.filterStruct = struct.struct;
          that.langArray = struct.lang;

          that.stateArray = struct.state;
          for (const value of struct.poet) {
            that.authorVisible[value.Id] = true;
          }
          that.poetArray = struct.poet;
          for (const value of that.poetArray) {
            that.modelFilter.autors[value.Id] = false;
          }
          that.changeModel(null);
          that.loadSelectPicker();
        }
      }

    };
    this.notfychangeservice.loadFilterStuct(callback, 'struct');
  }
  loadSelectPicker() {

    $(() => {
      $('.selectpicker').selectpicker();
    });
  }
  changeModel(value) {
    let filter = 'Nivo = "11c3c4bb-3f57-11ea-8116-81991b6bff66"';
    if (value === null || value === 'state' || value === 'lang') {
      this.changeAuthorModel();
    }
    if (this.modelFilter.search !== '') {
      const add = '(Pesnik__NazivPesnik contains "' + this.modelFilter.search
        + '" OR Pesem__NaslovPesmi contains "' + this.modelFilter.search + '")';
      filter = this.joinFilter(filter, add);
    }
    if (this.modelFilter.lang != null && this.modelFilter.lang.length > 0) {

      filter = this.joinFilter(filter, 'Jezik IN ["' + this.modelFilter.lang.join('","') + '"]');
    } else {
      const array = [];
      // tslint:disable-next-line: no-shadowed-variable
      for (const value of this.langArray) {
        array.push(value.Id);
      }

      filter = this.joinFilter(filter, 'Jezik IN ["' + array.join('","') + '"]');
    }
    if (this.modelFilter.state != null && this.modelFilter.state.length > 0) {

      filter = this.joinFilter(filter, 'Pesnik__Drzava IN ["' + this.modelFilter.state.join('","') + '"]');
    }
    const arrayPoet = [];
    for (const key in this.modelFilter.autors) {
      if (this.modelFilter.autors[key] === true) {
        arrayPoet.push(key);
      }
    }
    if (arrayPoet.length > 0) {
      filter = this.joinFilter(filter, 'Pesnik IN ["' + arrayPoet.join('","') + '"]');
    }
    const orderby = 'Pesem__NaslovPesmi ' + this.orderObj.pesem + ', Pesnik__NazivPesnik '
    + this.orderObj.pesnik + ',Interpreter__NazivInterpreter ' + this.orderObj.interpret;
    this.notfychangeservice.setToLocalStorage('filterModel', this.modelFilter);
    this.notfychangeservice.reloadPoetry(filter,orderby);
  }
  private joinFilter(filter, add) {
    if (filter !== '') {
      filter += ' AND ';
    }
    return filter + add;
  }
  changefilter() {
    if (!$('#searchFilter').hasClass('filter-is-visible')) {
      $('#searchFilter').addClass('filter-is-visible');
      $('#mainGrid').addClass('filter-is-visible');
      $(() => {
        $('.selectpicker').selectpicker();
      });
    } else {
      $('#searchFilter').removeClass('filter-is-visible');
      $('#mainGrid').removeClass('filter-is-visible');

    }
  }
  changeAuthorModel() {
    let languages = [];
    let states = [];
    if (this.modelFilter.lang != null && this.modelFilter.lang.length > 0) {
      languages = this.modelFilter.lang;
    } else {
      for (const value of this.langArray) {
        languages.push(value.Id);
      }
    }
    if (this.modelFilter.state != null && this.modelFilter.state.length > 0) {
      states = this.modelFilter.state;
    } else {
      for (const value of this.stateArray) {
        states.push(value.Id);
      }
    }
    const poets = {};
    for (const lang of languages) {
      if (!this.filterStruct[lang] || this.filterStruct[lang] === null) {
        continue;
      }
      const langLevel = this.filterStruct[lang];
      for (const state of states) {
        if (!langLevel[state] || langLevel[state] === null) {
          continue;
        }
        const poetLevel = langLevel[state];
        if (!poetLevel) {
          continue;
        }
        for (const poet in poetLevel) {
          if (poetLevel.hasOwnProperty(poet)) {
            poets[poet] = true;
          }
        }
      }
    }
    for (const key in this.authorVisible) {
      if (!this.authorVisible.hasOwnProperty(key)) {
        continue;
      }
      const visible = poets[key] !== null && poets[key];
      this.authorVisible[key] = visible;
      if (!visible && this.modelFilter.autors[key]) {
        delete this.modelFilter.autors[key];
      }
    }

  }
  changeSort(value) {
    switch (value) {
      case 'pesem': {
        this.__changeSort('pesem', '#sortPesem');
        break;
      }
      case 'pesnik': {
        this.__changeSort('pesnik', '#sortPesnik');
        break;
      }
      case 'interpret': {
        this.__changeSort('interpret', '#sortInterperet');
        break;
      }
    }
    this.changeModel('orderby');
  }
  __changeSort(key, id) {
    if (this.orderObj[key] === 'asc') {
      this.orderObj[key] = 'desc';
      $(id).addClass('fa-sort-alpha-desc');
      $(id).removeClass('fa-sort-alpha-asc');

    }
    else if (this.orderObj[key] === 'desc') {
      this.orderObj[key] = 'asc';
      $(id).addClass('fa-sort-alpha-asc');
      $(id).removeClass('fa-sort-alpha-desc');

    }
  }


}
