export class PoetrySource {
  Id: string;
  Pesnik: string;
  Pesnik__NazivPesnik: string;
  Interpreter: string;
  Interpreter__NazivInterpreter: string;
  Pesem: string;
  Pesem__NaslovPesmi: string;
  Jezik: string;
  Vir: string;
  Vrsta: string;

  _Separator: string;
  stream: string;
  constructor(rowData: any, baseUrl: string) {
    if(rowData === null)  {
      this.Id = null;
      this.Pesnik = null;
      this.Interpreter = null;
      this.Interpreter__NazivInterpreter = null;
      this.Pesem = null;
      this.Pesem__NaslovPesmi = null;
      this.Jezik = null;
      this.Vir = null;
      this.Vrsta = null;
      this._Separator = null;
      return;
    }
    this.Id = rowData.Id;
    this.Pesnik = rowData.Pesnik;
    this.Pesnik__NazivPesnik = rowData.Pesnik__NazivPesnik
    this.Interpreter = rowData.Interpreter;
    this.Interpreter__NazivInterpreter = rowData.Interpreter__NazivInterpreter;
    this.Pesem = rowData.Pesem;
    this.Pesem__NaslovPesmi = rowData.Pesem__NaslovPesmi;
    this.Jezik = rowData.Jezik;
    this.Vir = rowData.Vir;
    this.Vrsta = rowData.Vrsta;
    this._Separator = '---';
    this.stream = baseUrl + this.Vir;
    this.stream = this.stream.replace('Stream', 'Public');
    this.stream = this.stream.replace('?inline=true', '');
  }
}
