import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotifyChangesService } from '../notify-changes.service';
@Component({
  selector: 'app-error-template',
  templateUrl: './error-template.component.html',
  styleUrls: ['./error-template.component.css']
})
export class ErrorTemplateComponent implements OnInit {
  errorMessage = '';
  constructor(private notfychangeservice: NotifyChangesService) {
    this.errorMessage = this.notfychangeservice.getErrorMessage();

   }

  ngOnInit(): void {
  }

}
