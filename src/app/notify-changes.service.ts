import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
declare const E2Data: any;
declare const E2Extension: any;
declare const E2Auth: any;
declare const RpcClient: any;
declare const jQuery: any;
declare const $: any;
@Injectable({
  providedIn: 'root'
})
export class NotifyChangesService {
  private expiration = 28800000;
  // private baseUrl = '/harfa/';
  private baseUrl = 'https://dev.dopinus.com/harfa/';
  private cors = true;

  private actionType = new BehaviorSubject<any>('loader');
  private changeLogin = new BehaviorSubject<any>(false);
  private payedLanguages = new BehaviorSubject<any>(null);
  private poetry = new Subject<any>();
  private currentPoetry = new Subject<any>();
  private detailPoetry = new BehaviorSubject<any>(null);
  private langModel = new BehaviorSubject<any>({});
  private errorMessage = null;
  constructor() {
    RpcClient.config = {};
    RpcClient.config.url = this.getBaseUrl() + 'json.rpc';
    RpcClient.config.jsonp = false;
    RpcClient.config.cors = true;
    this.detailPoetry.next(this.getFromLocalStorage('detailobj'));
  }
  getBaseUrl() {
    return this.baseUrl;
  }
  // START auth section
  SignOut() {
    const auth = new E2Auth();

    auth.callback = () => {
      this.changeLogin.next(false);
    };
    auth.cbCtx = this;
    auth.SignOut();
  }
  IsAuth() {
    const e2Auth = new E2Auth();
    e2Auth.callback = (ctx, isAuth: boolean) => {
      this.changingStatusOfLogin(isAuth);
    };
    e2Auth.cbCtx = this;
    e2Auth.IsAuth();
  }
  Authenticate(username: string, password: string, persist: boolean) {
    const auth = new E2Auth();
    auth.username = username;
    auth.password = password;
    auth.orgid = '2b5979b8-2ad0-11ea-8116-81991b6bff66'; // harfa
    if (persist === true) {
      auth.persist = true;
    }
    auth.callback = (authStatus: boolean) => {
      if (authStatus === true) {
        this.pushMessage('Obvestilo', 'Prijava je bila uspešna.');
      } else {
        this.pushMessage('Napaka', 'Prijava je bila neuspešna!');
      }
      this.changingStatusOfLogin(authStatus);
    };
    auth.cbCtx = this;
    auth.Authenticate();
  }
  changeStatusOfLogin() {
    return this.changeLogin.asObservable();
  }
  private changingStatusOfLogin(authStatus: boolean) {
    if (authStatus === false) {
      this.changeActionType('login');
    }

    if (this.changeLogin.getValue() === authStatus) {

      return;
    }
    if (authStatus === true) {
      this.checkIfHasSettings();
    }
    this.changeLogin.next(authStatus);
  }
  changeActionType(type: string) {
    this.setToLocalStorage('actionType', type);
    if (this.actionType.getValue() === type) {
      return;
    }
    this.actionType.next(type);
  }
  listenActionType() {
    return this.actionType.asObservable();
  }
  getActionType() {
    return this.actionType.getValue();
  }
  // END auth section
  // START translation
  changeLangModel(data) {
    this.langModel.next(data);
  }
  reloadLangModel() {
    return this.langModel.asObservable();
  }
  getTranData() {
    return this.langModel.getValue();
  }
  // END translation

  reloadingPoetry() {
    return this.poetry.asObservable();
  }
  changeActivePoetry(obj) {
    return this.currentPoetry.next(obj);
  }
  changePoetryPlaying() {
    return this.currentPoetry.asObservable();
  }
  changeDetail(obj) {
    this.setToLocalStorage('detailobj', obj);
    this.detailPoetry.next(obj);
    this.changeActionType('detail');
  }
  reloadDetail() {
    return this.detailPoetry.asObservable();
  }
  getDetail() {
    return this.detailPoetry.getValue();
  }
  reloadPoetry(filter, orderby) {
    const e2data = new E2Data();
    e2data.entity = 'AnniEdicodoo/HarfViri';
    e2data.filter = filter;
    e2data.orderBy = orderby;
    e2data.format = 'Full';
    e2data.callback = (data) => {
      if (data.result) {
        const poet = data.result;
        this.poetry.next(poet);
      }
    };
    e2data.cbCtx = this;
    e2data.GetData();

  }
  pushMessage(caption, message) {
    if(caption=='Napaka'){
      alert(message);
    }
  }
  loadScript(array: Array<string>) {
    for (const value of array) {
      const body = document.body as HTMLDivElement;
      const script = document.createElement('script');
      script.innerHTML = '';
      script.src = value;
      script.async = false;
      script.defer = true;
      body.appendChild(script);
    }
  }
  loadFilterStuct(callback: any, typeOfStruct: string) {
    const e2ext = new E2Extension();
    e2ext.extension = 'harfafilterstruct';
    e2ext.args = { type: typeOfStruct };
    e2ext.callback = (response) => {
      if (typeOfStruct === 'auth') {
        if (response.result && response.result.status === 'ok') {
          this.changeLangModel(response.result.data.tran);
          this.payedLanguages.next(response.result.langpayed);
        }
      }
      if (response.error) {
        if (response.error.code && response.error.code === 401) {
          this.changingStatusOfLogin(false);
        }
      }
      callback(response);
    };
    e2ext.cbCtx = this;
    e2ext.Execute();
  }

  loadDetailRelatedContent(idAuthor: string, idInter: string, callback: any) {
    let filter = 'Nivo = "11c3c4bb-3f57-11ea-8116-81991b6bff66" AND Pesnik =  "' + idAuthor + '"';
    if (idInter !== null) {
      filter += ' AND Interpreter = "' + idInter + '"';
    }
    const e2Data = new E2Data();
    e2Data.entity = 'AnniEdicodoo/HarfViri';
    e2Data.filter = filter;
    e2Data.format = 'Full';
    e2Data.callback = callback;
    e2Data.cbCtx = this;
    e2Data.GetData();
  }

  // START local storage
  setToLocalStorage(key: string, obj: any) {
    const now = new Date();
    const item = {
      value: obj,
      expiry: now.getTime() + this.expiration
    };
    localStorage.setItem(key, JSON.stringify(item));
  }
  getFromLocalStorage(key: string) {
    const itemStr = localStorage.getItem(key);
    if (itemStr === null) {
      return null;
    }
    const item = JSON.parse(itemStr);
    const now = new Date();
    // compare the expiry time of the item with the current time
    if (now.getTime() > item.expiry) {
      // If the item is expired, delete the item from storage
      // and return null
      localStorage.removeItem(key);
      return null;
    }
    return item.value;
  }
  // END local storage
  private transalteSelect(def: any, trans: any) {
    if (trans === null || !trans) {
      return def;
    }
    const keys = ['noneSelectedText', 'noneResultsText', 'countSelectedText', 'selectAllText', 'deselectAllText'];
    for (const key of keys) {
      const keyJoined = 'Select_' + key;
      if (!trans.hasOwnProperty(keyJoined)) {
        continue;
      }
      def[key] = trans[keyJoined];
    }
    if (trans.hasOwnProperty('Select_excedded') && trans.hasOwnProperty('Select_exceddedGroup')) {
      def.maxOptionsText = (numAll, numGroup) => {
        return [
          trans.Select_excedded,
          trans.Select_exceddedGroup,
        ]
      }
    }
    return def;
  }
  private checkIfHasSettings() {
    const that = this;
    const callback = (res: any) => {
      if (res.result) {
        if (res.result.status === 'ok') {
          const data = res.result.data;
          let def = {
            noneSelectedText: 'Nič izbranega',
            noneResultsText: 'Ni zadetkov za {0}',
            countSelectedText: '{0} od {1} izbranih',
            maxOptionsText: (numAll, numGroup) => {
              return [
                'Omejitev dosežena (max. izbranih: {n})',
                'Omejitev skupine dosežena (max. izbranih: {n})'
              ];
            },
            selectAllText: 'Izberi vse',
            deselectAllText: 'Počisti izbor',
            multipleSeparator: ', '
          };
          const trans = data.tran;
          def = that.transalteSelect(def, trans);

          // tslint:disable-next-line: only-arrow-functions
          (function ($) {
            $.fn.selectpicker.defaults = def;
          })(jQuery);
          let locstoreAT = that.getFromLocalStorage('actionType');
          if (locstoreAT !== null) {
            if (locstoreAT === 'error' || locstoreAT === 'login') {
              locstoreAT = 'show';
            }
          } else {
            locstoreAT = 'show';
          }

          that.changeActionType(locstoreAT);
          // that.notfychangeservice.changeActionType('show');
        } else if (res.result.status === 'error') {
          switch (res.result.errorCode) {
            case '000': {
              that.errorMessage = 'Type of request is not defined!';
              that.changeActionType('error');
              break;
            }
            case '001': {
              that.errorMessage = 'Device has not setted settings!';
              that.changeActionType('error');
              break;
            }
            case '002': {
              that.errorMessage = 'Device has multiple settings defined!';
              that.changeActionType('error');
              break;
            }
          }
        }
      } else {
        that.errorMessage = res.error.data;
        that.changeActionType('error');
      }
    };
    this.loadFilterStuct(callback, 'auth');
  }
  getErrorMessage() {
    return this.errorMessage;
  }
}
