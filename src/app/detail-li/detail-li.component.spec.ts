import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailLiComponent } from './detail-li.component';

describe('DetailLiComponent', () => {
  let component: DetailLiComponent;
  let fixture: ComponentFixture<DetailLiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailLiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailLiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
