import { Component, OnDestroy, ViewChild, Renderer2, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotifyChangesService } from '../notify-changes.service';
import { MatVideoComponent } from 'mat-video/app/video/video.component';
import {PoetrySource} from '../poetry-source';
declare const $: any;
declare const WaveSurfer: any;

@Component({
  selector: 'app-detail-li',
  templateUrl: './detail-li.component.html',
  styleUrls: ['./detail-li.component.css']
})
export class DetailLiComponent implements AfterViewInit, OnDestroy {

  @ViewChild('video') matVideo: MatVideoComponent;
  video: HTMLVideoElement;
  poetObj: PoetrySource;
  poetryObj : PoetrySource;
  relatedContent = [];
  relatedContentObj = null;
  poetryUrl = null;
  wavesurfer = null;
  subscriptiondetail: Subscription;
  play = false;
  loaded = { video: false, wave: false };


  constructor(private notfychangeservice: NotifyChangesService, private renderer: Renderer2) {
    this.poetObj = new PoetrySource(null, null);
    this.poetryObj = new PoetrySource(null, null);
    this.subscriptiondetail = this.notfychangeservice.reloadDetail().subscribe(detailObj => {
      this.poetObj = new PoetrySource(detailObj , this.notfychangeservice.getBaseUrl());
      this.changePoetry(detailObj);
    });
    if (this.notfychangeservice.getDetail() !== null) {
      //this.changePoetry(this.notfychangeservice.getDetail());
    }

  }

  ngAfterViewInit(): void {

    const that = this;
    this.video = this.matVideo.getVideoTag();
    this.matVideo.loop = false;

  }
  ngOnDestroy() {
    this.subscriptiondetail.unsubscribe();
    if (this.wavesurfer !== null) {
      this.wavesurfer.pause();
      this.wavesurfer.destroy();
    }
  }
  setPlay(obj: any) {

    this.poetryObj = new PoetrySource(obj, this.notfychangeservice.getBaseUrl());
    this.poetryUrl = this.poetryObj.stream; // wavePlayerMain
    // this.poetryUrl = '/music-files/rrr.mp4';
    console.log(this.poetryUrl);
    this.changePlayPoetry();
  }
  changePoetry(detailObj: any) {
    if (this.poetObj != null) {
      if (this.poetObj.Id === detailObj.Id && this.relatedContentObj != null) {
        return;
      }
    }
    let interId = null;

    if (this.poetObj.Interpreter__NazivInterpreter !== 'Neznan') {
      interId = this.poetObj.Interpreter;
    }
    const that = this;
    const callback = (response: any) => {
      if (response.result) {
        that.relatedContent = response.result.data;
        const obj = {};
        for (const value of that.relatedContent) {
          obj[value.Id] = value;
          value.classimage = 'audio-img';
          if (value.Vrsta === '384103d4-3f57-11ea-8116-81991b6bff66') {
            value.classimage = 'video-img';
          }
        }
        that.relatedContentObj = obj;
        that.setPlay(detailObj);
      } else {

      }

    };
    this.notfychangeservice.loadDetailRelatedContent(this.poetObj.Pesnik, interId, callback)
  }
  getTypeOfFile()
  {
    if(this.poetryObj.Vrsta==='2f6ab76d-3f57-11ea-8116-81991b6bff66') {
      return 'audio';
    }
    return 'video';
  }
  backOnList() {
    this.notfychangeservice.changeActionType('show');
    this.ngOnDestroy();
  }
  changePlay(id) {
    this.setPlay(this.relatedContentObj[id]);
  }
  changePlayPoetry() {
    this.loaded.video = false;
    this.loaded.wave = false;
    const that = this;

    if (this.wavesurfer === null) {
      $(document).ready(() => {
        that.wavesurfer = WaveSurfer.create({
          container: '#audiowaveDetail',
          waveColor: '#d0d7db',
          progressColor: '#9ca1a4',
          barWidth: 2,
          barGap: 1,
          cursor: true,
          responsive: true,
          cursorWidth: 0,
          height: 80,
          barHeight: 0.7,
        });
        that.wavesurferInit();
        that.wavesurfer.setMute(true);
        that.wavesurfer.on('seek', (e) => {
          let paused = false;
          if (that.video.paused === true)  {
            paused = true;
          } else {
            that.pausePlayer();
          }
          const seek = Math.round(that.video.duration * e);
          that.video.currentTime = seek;
          const videoElement = that.video;
          if (paused)  {
            return;
          }
          const timer = setInterval(() => {
            if (videoElement.paused && videoElement.readyState === 4
              || !videoElement.paused) {
              that.playPlayer();
              clearInterval(timer);
            }
          }, 50);
        });
        that.wavesurfer.on('ready', () => {
          that.loaded.wave = true;
          console.log('waveReady');
          that.playAfterLoading();
        });

        that.video.onloadeddata = () => {
          that.loaded.video = true;
          console.log('videoReady');
          that.playAfterLoading();
        };
        that.video.onended = () => {
            that.wavesurfer.seek = 0;
            that.play = false;
        };
        that.wavesurfer.load(this.poetryUrl);
        that.video.src = this.poetryUrl;
      });

    } else {
      this.pausePlayer();
      this.wavesurfer.load(this.poetryUrl);
      that.video.src = this.poetryUrl;
    }
  }
  playAfterLoading() {
    console.log(this.loaded);
    if (this.loaded.video === true && this.loaded.wave === true) {
      if(this.getTypeOfFile()==='audio') {
        this.video.poster='/harfa/Public/e09399fa-b990-400d-b7e9-8899acddb53d';
        this.matVideo.poster='/harfa/Public/e09399fa-b990-400d-b7e9-8899acddb53d';
        console.log('set poster');
      }   else {
        this.matVideo.poster=null;
        this.video.poster=null;
        console.log('remuve poster');
      }
      this.playPlayer();
    }
  }
  playPlayer() {
    this.wavesurfer.play();
    this.video.play();
    this.play = true;

  }
  pausePlayer() {
    this.wavesurfer.pause();
    this.video.pause();
    this.play = false;
  }

  wavesurferInit() {
    const wavesurfer = this.wavesurfer;
    if (wavesurfer === null) {
      return;
    }
    const that = this;
    $(document).ready(() => {
      $('#currentTime').appendTo('wave wave');
    });

    wavesurfer.on('audioprocess', () => {
      const clipCurrentTime = wavesurfer.getCurrentTime();
      document.getElementById('currentTime').innerHTML = that.formatSecondsAsTime(clipCurrentTime);
    });

    wavesurfer.on('ready', () => {
      const clipTime = wavesurfer.getDuration();
      document.getElementById('clipTime').innerHTML = that.formatSecondsAsTime(clipTime);
    });

    wavesurfer.on('interaction', () => {
      const clipCurrentTime = wavesurfer.getCurrentTime();
      document.getElementById('currentTime').innerHTML = that.formatSecondsAsTime(clipCurrentTime);
    });

    wavesurfer.on('play', () => {
      $(document).ready(() => {
        $('.play-box').addClass('playing');
        $('.jplayer').jPlayer('pauseOthers');
      });
    });

    wavesurfer.on('pause', () => {
      $(document).ready(() => {
        $('.play-box').removeClass('playing');
      });
    });

  }
  formatSecondsAsTime(secs: any) {
    const hr = Math.floor(secs / 3600);
    const min = Math.floor((secs - (hr * 3600)) / 60);
    const sec = Math.floor(secs - (hr * 3600) - (min * 60));
    let smin = '' + min;
    const shr = '' + hr;
    let ssec = '' + sec;
    if (min < 10) {
      smin = '0' + min;
    }
    if (sec < 10) {
      ssec = '0' + sec;
    }
    return smin + ':' + ssec;

  }
  playdetail() {
    if (this.wavesurfer !== null) {
      if (this.play === false) {
        this.playPlayer();
      } else {
        this.pausePlayer();
      }

    }
  }

}

