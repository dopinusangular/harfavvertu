import { BrowserModule } from '@angular/platform-browser';

import { MatVideoModule } from 'mat-video';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ListFilteredComponent } from './list-filtered/list-filtered.component';
import { FilterComponent } from './filter/filter.component';
import { CurrentPlayingComponent } from './current-playing/current-playing.component';
import { CommonPanelComponent } from './common-panel/common-panel.component';
import { DetailLiComponent } from './detail-li/detail-li.component';
import { ErrorTemplateComponent } from './error-template/error-template.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListFilteredComponent,
    FilterComponent,
    CurrentPlayingComponent,
    CommonPanelComponent,
    DetailLiComponent,
    ErrorTemplateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatVideoModule
  ],
  providers: [

],
  bootstrap: [AppComponent]
})
export class AppModule { }

