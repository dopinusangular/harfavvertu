import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotifyChangesService } from '../notify-changes.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnDestroy {
  subscription: Subscription;
  username = null;
  password = null;
  persist = false;
  constructor(private notfychangeservice: NotifyChangesService) {

   }

  ngOnDestroy() {
  }
  Authenticate() {
    this.notfychangeservice.Authenticate(this.username, this.password, this.persist);
  }
}
