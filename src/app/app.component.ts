import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotifyChangesService } from './notify-changes.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'harfavvetru';
  actionType = 'loader';
  errorMessage = null;
  subscription: Subscription;
  subscriptionActionType: Subscription;
  subscriptiondetail: Subscription;
  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscriptionActionType = this.notfychangeservice.listenActionType().subscribe(actionType => {
      this.actionType = actionType;


    });
    this.actionType = this.notfychangeservice.getActionType();
    this.notfychangeservice.IsAuth();
  }

}
